# -*- coding: utf-8 -*-

# Scrapy settings for ttkto project
#
# All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

BOT_NAME = 'ttkto'

SPIDER_MODULES = ['ttkto.spiders']
NEWSPIDER_MODULE = 'ttkto.spiders'
ITEM_PIPELINES = {'ttkto.pipelines.FormatPipeline' : 100 \
                 ,'ttkto.pipelines.SQLPipeline' : 200 
                  }
# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'tutorial (+http://www.yourdomain.com)'
