# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class SzakItem(scrapy.Item):
    szak = scrapy.Field()
    link = scrapy.Field()
    szakazon = scrapy.Field()
    tagozat = scrapy.Field()
    evfolyam = scrapy.Field()

class KurzItem(scrapy.Item):
    kurzazon = scrapy.Field()
    kurzkod = scrapy.Field()
    kurzus = scrapy.Field()
    elo = scrapy.Field()
    gyak = scrapy.Field()
    tipus = scrapy.Field()
    link = scrapy.Field()
    szakazon= scrapy.Field()
    evfolyam = scrapy.Field()

class TanarItem(scrapy.Item):
    tanarnev = scrapy.Field()
    kurzazon = scrapy.Field()
    csopnum = scrapy.Field()

class HelyItem(scrapy.Item):
    nap = scrapy.Field()
    kezd = scrapy.Field()
    vege = scrapy.Field()
    epulet = scrapy.Field()
    terem = scrapy.Field()
    kurzazon = scrapy.Field()
    csopnum = scrapy.Field()
    kiirva = scrapy.Field()

