# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html

import scrapy
import logging
from scrapy.exceptions import DropItem
import mysql.connector
import ttkto.items
import os
import datetime

#file for storing connection details in one variable called conn_details
from conn_data import *

class SQLPipeline(object):
    
    def process_item(self, item, spider):
        if(type(item) == ttkto.items.SzakItem):
            self.process_szak(item)
        elif(type(item) == ttkto.items.KurzItem):
            self.process_kurz(item)
        elif(type(item) == ttkto.items.TanarItem):
            self.process_tanar(item)
        elif(type(item) == ttkto.items.HelyItem):
            self.process_hely(item)
        return item

    def process_szak(self, item):
        self.c.execute("INSERT INTO szakok VALUES (%s,%s,%s,%s,%s)", (
             item["szak"], item["tagozat"], item["evfolyam"], item["link"], item["szakazon"]))

    def process_kurz(self, item):
        self.c.execute("INSERT INTO kurzusok VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s)", (
            item["kurzazon"], item["kurzkod"], item["kurzus"], item["elo"], item["gyak"], item["tipus"], 
            item["link"], item["szakazon"], item["evfolyam"]))
        
    def process_tanar(self, item):
        self.c.execute("INSERT INTO tanarok VALUES (%s,%s,%s)", (item["tanarnev"], item["kurzazon"], item["csopnum"]))

    def process_hely(self, item):
        if item["kiirva"] == 0:
            self.c.execute("INSERT INTO helyek VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s)", ("", "", "", "", "", item["kurzazon"],
            item["csopnum"], 0, item["kiirva"]))
        else:
            for i in range(len(item["nap"])):
                self.c.execute("INSERT INTO helyek VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s)", (item["nap"][i], item["kezd"][i], item["vege"][i], item["epulet"][i], item["terem"][i], item["kurzazon"], item["csopnum"], i, item["kiirva"]))

    
    def open_spider(self, spider):
	os.popen("mysqldump -u {} --password={} {} > {}-ttkto.sql".format(conn_details['user'], conn_details['password'], conn_details['database'], datetime.datetime.now().strftime("%y%m%d-%H%M")))
        self.conn = mysql.connector.connect( **conn_details )
        self.c = self.conn.cursor()
        self.c.execute("DROP TABLE IF EXISTS szakok, kurzusok, tanarok, helyek;")
        self.c.execute("CREATE TABLE szakok (szak TEXT, tagozat TEXT, evfolyam INTEGER, link TEXT, szakazon TEXT)")
        self.c.execute("CREATE TABLE kurzusok (kurzazon TEXT, kurzkod TEXT, kurzus TEXT, elo INTEGER, gyak INTEGER, tipus TEXT,\
                                             link TEXT, szakazon TEXT, evfolyam INTEGER)")
        self.c.execute("CREATE TABLE tanarok (tanarnev TEXT, kurzazon TEXT, csopnum INTEGER)")
        self.c.execute("CREATE TABLE helyek (nap TEXT, kezd TIME, vege TIME,\
                epulet TEXT, terem TEXT, kurzazon TEXT, csopnum INT, oraszam INT, kiirva INT)")
        self.conn.commit()
        logging.info("db opend")

    def close_spider(self, spider):
        self.conn.commit()
	self.c.close()
        self.conn.close()
        logging.info("db closed")

class FormatPipeline(object):

    def process_item(self, item, spidre):
        if(type(item) == ttkto.items.SzakItem):
            item = self.process_szak(item)
        elif(type(item) == ttkto.items.KurzItem):
            item = self.process_kurz(item)
        elif(type(item) == ttkto.items.TanarItem):
            item = self.process_tanar(item)
        elif(type(item) == ttkto.items.HelyItem):
            item = self.process_hely(item)
        return(item)

    def process_szak(self, item):
        item["evfolyam"] = int(item["evfolyam"])
        if (item["evfolyam"] < 1) or (item["evfolyam"] > 10):
            #0 should not exist, but not sure on upper limit
            raise DropItem("Evfolyam %i" % item["evfolyam"])
        if item["tagozat"] not in ["N", "L", "E"]:
            raise DropItem("Tagozat %s" % item["tagozat"])
        return(item)

    def process_kurz(self, item):
        item["evfolyam"] = int(item["evfolyam"])
        if (item["evfolyam"] < 1) or (item["evfolyam"] > 10):
            #0 should not exist, but not sure on upper limit
            raise DropItem("Evfolyam %i" % item["evfolyam"])
        return(item)

    def process_tanar(self, item):
        return(item)

    def process_hely(self, item):
        
        if(item["nap"] == []):
            item["kiirva"] = 0
        if(item["kezd"] == []):
            item["kiirva"] = 0
        if(item["vege"] == []):
            item["kiirva"] = 0
        if(item["epulet"] == []):
            item["kiirva"] = 0
        if(item["terem"] == []):
            item["kiirva"] = 0

        std_len = len(item["nap"])
        for i in ["kezd","vege","epulet","terem"]:
            if len(item[i]) != std_len:
                logging.info("Malformed hely:" + item["kurzazon"] + ":" + item["csopnum"])
                item["kiirva"] = 0

        return(item)

class XMLPipeline(object):

    def process_item(self, item, spidre):
        string = u"<item>"
        
        if(type(item) == ttkto.items.SzakItem):
            string += "<type>szak</type>"
        elif(type(item) == ttkto.items.KurzItem):
            string += "<type>kurz</type>"
        elif(type(item) == ttkto.items.TanarItem):
            string += "<type>tanar</type>"
        elif(type(item) == ttkto.items.HelyItem):
            string += "<type>hely</type>"

        for i in item.items():
            string += "<" + i[0] + ">" + i[1] + "</" + i[0] + ">"
        string += "</item>\n"
        print(string.encode("utf-8"))
        self.file.write(string.encode("utf-8"))
        return(item)

    def open_spider(self, spider):
        self.file = open("test1.xml","w")

    def close_spider(self, spider):
        self.file.close()


