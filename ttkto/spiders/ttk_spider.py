import scrapy
import ttkto.items
import re

class TTKSpider(scrapy.Spider):
    name = "ttkscrape"
    start_urls = [
            "http://ttkto.elte.hu/scripts/tanrende/szaklista.idc?tanevfelev=2015-2016-1" ]
    def parse(self, response):
        ret = []
        for link in response.xpath("//tr/td/a"):
            #geting all szak
            item = ttkto.items.SzakItem()
            item["szak"] = link.xpath("text()").extract()[0]
            href = link.xpath("@href")
            #extract useful stuff from link
            item["link"] = href.extract()[0]
            item["szakazon"] = href.re(r'szakparkodp=(.{4})')[0]
            item["tagozat"] = href.re(r'tagozatp=(.)')[0]
            item["evfolyam"] = href.re(r'evfolyamp=(\d+)')[0]
            if re.search("izik",item["szak"]):
                #only process fizika related szaks
                #create new request to get all kurzus from szak, whit meta to get unique szak for each kurz
                request = scrapy.Request("http://ttkto.elte.hu/scripts/tanrende/" + item["link"], callback = self.kurz_parse)
                request.meta["szakazon"] = item["szakazon"]
                request.meta["evfolyam"] = item["evfolyam"]
                ret += [request]
            ret += [item]
        return ret
    
    def kurz_parse(self, response):
        ret = []
        for row in response.xpath("//tr[position()>1]"):
            item = ttkto.items.KurzItem()
            item["kurzazon"] = row.xpath("td[position()=1]/a/text()").extract()[0]
            item["kurzkod"] = row.xpath("td[position()=2]/text()").extract()[0]
            item["kurzus"] = row.xpath("td[position()=3]/a/text()").extract()[0]
            item["elo"] = row.xpath("td[position()=4]/text()").extract()[0]
            item["gyak"] = row.xpath("td[position()=5]/text()").extract()[0]
            item["tipus"] = row.xpath("td[position()=8]/text()").extract()[0]
            item["link"] = row.xpath("td[position()=3]/a/@href").extract()[0]
            item["szakazon"] = response.meta["szakazon"]
            item["evfolyam"] = response.meta["evfolyam"]
            request = scrapy.Request("http://ttkto.elte.hu/scripts/tanrende/" + item["link"], callback = self.csop_parse)
            request.meta["kurzazon"] = item["kurzazon"] 
            ret += [item, request]
        return ret

    def csop_parse(self, response):
        ret = []
        for row in response.xpath("//tr[position()>1]"):
            csopnum = row.xpath("td[position()=2]/text()").extract()[0]
            helylink = row.xpath("td[position()=1]/a/@href").extract()[0]
            tanarlink = row.xpath("td[position()=4]/a/@href").extract()[0]
            kurzazon = response.meta["kurzazon"]
            helyreq = scrapy.Request("http://ttkto.elte.hu/scripts/tanrende/" + helylink, callback = self.hely_parse)
            helyreq.meta["kurzazon"] = kurzazon
            helyreq.meta["csopnum"] = csopnum
            tanreq = scrapy.Request("http://ttkto.elte.hu/scripts/tanrende/" + tanarlink, callback = self.tanar_parse)
            tanreq.meta["kurzazon"] = kurzazon
            tanreq.meta["csopnum"] = csopnum
            ret += [tanreq, helyreq]
        return(ret)

    def hely_parse(self, response):
        item = ttkto.items.HelyItem()
        item["nap"] = []
        item["kezd"] = []
        item["vege"] = []
        item["epulet"] = []
        item["terem"] = []
        item["kurzazon"] = response.meta["kurzazon"]
        item["csopnum"] = response.meta["csopnum"]
        item["kiirva"] = 0
        kiirva = 0;
        for row in response.xpath("//tr[position()>1]"):
            kiirva = 1;
            if(row==[]):
                logging.info("Nincs kiirva: " + response.meta["kurzazon"] + " : " + response.meta["csopnum"])
                item["kiirva"] = 0
                return(item)
            nap = row.xpath("td[position()=1]/text()").extract()
            kezd = row.xpath("td[position()=2]/text()").extract()
            vege =  row.xpath("td[position()=3]/text()").extract()
            epulet = row.xpath("td[position()=4]/text()").extract()
            terem = row.xpath("td[position()=5]/text()").extract()
            if nap != []:
                item["kiirva"] = 1;
                item["nap"].append(nap[0])
            else:
                item["nap"].append("")
            if kezd != []:
                item["kiirva"] = 1;
                item["kezd"].append(kezd[0])
            else:
                item["kezd"].append("")
            if vege != []:
                item["kiirva"] = 1;
                item["vege"].append(vege[0])
            else:
                item["vege"].append("")
            if epulet != []:
                item["kiirva"] = 1;
                item["epulet"].append(epulet[0])
            else:
                item["epulet"].append("")
            if terem != []:
                item["kiirva"] = 1;
                item["terem"].append(terem[0])
            else:
                item["terem"].append("")
        if kiirva == 0:
            logging.info("Nincs kiirva: " + response.meta["kurzazon"] + " : " + response.meta["csopnum"])
        return(item)

    def tanar_parse(self, response):
        ret = []
        for row in response.xpath("//tr[position()>1]"):
            item = ttkto.items.TanarItem()
            item["tanarnev"] = row.xpath("td[position()=1]/text()").extract()[0]
            item["kurzazon"] = response.meta["kurzazon"]
            item["csopnum"] = response.meta["csopnum"]
            ret += [item]
        return(ret)


